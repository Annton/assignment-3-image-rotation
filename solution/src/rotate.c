#include "rotate_util.h"


//
// Created by anton on 26.12.2023.
//
struct image rotate(struct image* const source, int64_t angle ){
    angle=(angle+360)%360;
    switch (angle) {
        case 0:
            return rotate0(source);
        case 90:
            return rotate90(source);
        case 180:
            return rotate180(source);
        case 270:
            return rotate270(source);
        default:
            return *source;

    }

}

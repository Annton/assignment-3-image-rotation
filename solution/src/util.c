//
// Created by anton on 28.12.2023.
//
#include "util.h"
#include <stdlib.h>
enum validate_status validate_input(int argc, char* argv[]){
    if (argc!=COUNT_INPUT_ARGS){
        return VALIDATE_INVALID_SIGNATURE;
    }
    int angle=atoi(argv[3]);
    if (!(angle == -270 || angle == -180 || angle == -90 || angle == 0 || angle == 90 || angle == 180 || angle == 270)) {
        return VALIDATE_INVALID_ANGLE;
    }
    return VALIDATE_OK;

}

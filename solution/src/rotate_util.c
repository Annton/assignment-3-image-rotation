//
// Created by anton on 26.12.2023.
//
#include "rotate_util.h"
//works
struct image rotate0(struct image* const source){
    uint64_t new_width = source->width;
    uint64_t new_height = source->height;
    struct image rotated_image = init_image(new_width, new_height);
    for (size_t i = 0; i < source->height; ++i) {
        for (size_t j = 0; j < source->width; ++j) {
            rotated_image.data[i * source->width + j] = source->data[i * source->width + j];
        }
    }

    return rotated_image;
}
//works
struct image rotate90(struct image* const source){

    uint64_t new_width = source->height;
    uint64_t new_height = source->width;
    struct image rotated_image = init_image(new_width, new_height);
    for (size_t i = 0; i < source->height; ++i) {
        for (size_t j = 0; j < source->width; ++j) {
            rotated_image.data[((new_height - 1 - j) * new_width) + i] = source->data[(i * source->width) + j];
        }
    }

    return rotated_image;
}
//works
struct image rotate180(struct image* const source){
    uint64_t new_width = source->width;
    uint64_t new_height = source->height;
    struct image rotated_image = init_image(new_width, new_height);
    for (size_t i = 0; i < source->height; ++i) {
        for (size_t j = 0; j < source->width; ++j) {
            rotated_image.data[((new_height - 1 - i) * new_width) + (new_width - 1 - j)] = source->data[(i * source->width) + j];
        }
    }

    return rotated_image;
}
//works
struct image rotate270(struct image* const source){
    uint64_t new_width = source->height;
    uint64_t new_height = source->width;
    struct image rotated_image = init_image(new_width, new_height);
    for (size_t i = 0; i < source->height; ++i) {
        for (size_t j = 0; j < source->width; ++j) {
            rotated_image.data[(j * new_width) + (new_width - 1 - i)] = source->data[(i * source->width) + j];

        }
    }
    return rotated_image;
}

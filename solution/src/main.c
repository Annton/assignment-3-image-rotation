

#include "bmp.h"
#include "rotate.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {
    enum validate_status validate_status = validate_input(argc, argv);

    if (validate_status != VALIDATE_OK) {
        switch (validate_status) {
            case VALIDATE_INVALID_ANGLE:
                fprintf(stderr, "Incorrect angle value\n");
                break;
            case VALIDATE_INVALID_SIGNATURE:
                fprintf(stderr, "Incorrect input signature\n");
                break;
            default:
                fprintf(stderr, "Unexpected error ");
        }
        return 1;
    }

    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");
    int angle = atoi(argv[3]);

    if (!in) {
        fprintf(stderr, "Opening input file failed\n");
        return 1;
    }
    if (!out) {
        fprintf(stderr, "Opening output file failed\n");
        return 1;
    }

    struct image* image = malloc(sizeof(struct image));
    if (!image) {
        fprintf(stderr, "Failed to allocate memory\n");
        return 1;
    }

    enum read_status read_status = from_bmp(in, image);
    fclose(in);

    if (read_status != READ_OK) {
        switch (read_status) {
            case READ_INVALID_SIGNATURE:
                fprintf(stderr, "Invalid signature\n");
                break;
            case READ_INVALID_HEADER:
                fprintf(stderr, "Invalid header\n");
                break;
            case READ_INVALID_BITS:
                fprintf(stderr, "Invalid image data\n");
                break;
            default:
                fprintf(stderr, "Unexpected error ");
        }

        return 1;
    }

    struct image rotated_img = rotate(image, angle);
    free_image(image);

    if (to_bmp(out, &rotated_img) != WRITE_OK) {
        fprintf(stderr, "Error writing image data\n");
        free_image(&rotated_img); // Освобождение памяти для rotated_img
        return 1;
    }

    fclose(out);
    free_image(&rotated_img);
    free(image);
    return 0;
}

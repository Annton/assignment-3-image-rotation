//
// Created by anton on 26.12.2023.
//
#include "bmp.h"



struct bmp_header init_bmp_header(uint64_t width, uint64_t height){
    uint64_t padding= get_padding(width);
    uint64_t size_bmp_header= sizeof(struct bmp_header);
    uint64_t size_image=height*(padding+width)* sizeof( struct pixel);
    return (struct bmp_header){
            .bfType=CONST_BFTYPE,
            .bfileSize=size_bmp_header+size_image,
            .bfReserved=CONST_BFRESERVED,
            .bOffBits=size_bmp_header,
            .biSize=CONST_BISIZE,
            .biWidth=width,
            .biHeight=height,
            .biPlanes=CONST_BIPLANES,
            .biBitCount=CONST_BIBITCOUNT,
            .biCompression=CONST_BICOMPRESSION,
            .biSizeImage=size_image,
            .biXPelsPerMeter=CONST_BIXPELSPERMETER,
            .biYPelsPerMeter=CONST_BIYPELSPERMETER,
            .biClrUsed=CONST_BICLRUSED,
            .biClrImportant=CONST_BICLRIMPORTANT
    };
}
enum read_status from_bmp( FILE* in, struct image* img ) {
    uint64_t size_pixel = sizeof(struct pixel);
    uint64_t size_bmp_header = sizeof(struct bmp_header);


    struct bmp_header bmp_header;
    if (!fread(&bmp_header, size_bmp_header, 1, in)) {
        return READ_INVALID_HEADER;
    }
    if (bmp_header.bfType != CONST_BFTYPE) {
        return READ_INVALID_SIGNATURE;
    }
    uint64_t width = bmp_header.biWidth;
    uint64_t height = bmp_header.biHeight;
    uint64_t padding = get_padding(width);
    *img = init_image(width, height);
    for (size_t i = 0; i < height; i++) {
        if (!fread(img->data + i * width, size_pixel, width, in) ||
                fseek(in,  (long) padding,SEEK_CUR)) {
            free_image(img);
            return READ_INVALID_BITS;
        }

    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    uint64_t size_pixel = sizeof(struct pixel);
    uint64_t size_bmp_header = sizeof(struct bmp_header);
    uint64_t width=img->width;
    uint64_t height=img->height;
    struct bmp_header bmp_header=init_bmp_header(width,height);
    uint64_t padding= get_padding(width);
    if (!fwrite(&bmp_header,size_bmp_header,1, out)){
        return WRITE_ERROR;
    }
    for (size_t i=0; i<height;i++){
        if (!fwrite(img->data+i*width,size_pixel,width,out) ||
        fseek(out,(long) padding,SEEK_CUR)){
            return WRITE_ERROR;
        }

    }
    return WRITE_OK;

}


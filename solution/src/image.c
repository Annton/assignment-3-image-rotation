
#include "image.h"
#include <malloc.h>
//
// Created by anton on 26.12.2023.
//
uint64_t get_padding(uint64_t width){
    return width%PADDING_BYTES;
}

struct image init_image(uint64_t width, uint64_t height){
    struct pixel* pixels=(struct pixel*) malloc(width*height* sizeof(struct pixel));
    if (!pixels){
        return (struct image){0};
    }
    return (struct image){
        .data=pixels,
        .width=width,
        .height=height
    };
}
void free_image(struct image* img){
    free(img->data);
    img->data=NULL;
}


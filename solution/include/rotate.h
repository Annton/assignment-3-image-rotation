//
// Created by anton on 26.12.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "image.h"
#include <stdint.h>

struct image rotate(struct image* const source, int64_t angle );

#endif //IMAGE_TRANSFORMER_ROTATE_H

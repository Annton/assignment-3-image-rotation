//
// Created by anton on 26.12.2023.
//
#define PADDING_BYTES 4
#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct pixel { uint8_t b, g, r; };

uint64_t get_padding(uint64_t width);

struct image init_image(uint64_t width, uint64_t height);

void free_image(struct image* img);
#endif //IMAGE_TRANSFORMER_IMAGE_H

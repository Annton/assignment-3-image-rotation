//
// Created by anton on 26.12.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_UTIL_H
#define IMAGE_TRANSFORMER_ROTATE_UTIL_H
#include "image.h"
#include <stddef.h>

struct image rotate0(struct image* const source);

struct image rotate90(struct image* const source);

struct image rotate180(struct image* const source);

struct image rotate270(struct image* const source);

#endif //IMAGE_TRANSFORMER_ROTATE_UTIL_H

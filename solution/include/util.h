//
// Created by anton on 26.12.2023.
//
#define COUNT_INPUT_ARGS 4
#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H
enum validate_status{
    VALIDATE_OK=0,
    VALIDATE_INVALID_SIGNATURE,
    VALIDATE_INVALID_ANGLE
};
enum validate_status validate_input(int argc, char* argv[]);
#endif //IMAGE_TRANSFORMER_UTIL_H
